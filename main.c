#include <stdint.h>
#include <stdio.h>
#include "cortex.h"
#include "rtl8710.h"
#include "mask.h"
#include "serial.h"

volatile uint32_t timer;

void timer_init(){
	cortex_interrupt_disable(TIMER0_INT);

	timer = 0;

	PERI_ON->SOC_FUNC_EN |= PERI_ON_SOC_FUNC_EN_GTIMER; // enable timer peripheral
	PERI_ON->PESOC_CLK_CTRL |= PERI_ON_CLK_CTRL_ACTCK_TIMER_EN; // enable clock for timer peripheral
	PERI_ON->PESOC_CLK_CTRL |= PERI_ON_CLK_CTRL_SLPCK_TIMER_EN;
	TIMER->TIM0_LOAD_COUNT = 0xFFFF; // this is the value that the timer counter starts counting down from
	TIMER->TIM0_CONTROL = TIMER_CONTROL_MODE; // 1 - user-defined count mode, 0 - free-running mode
	TIMER->TIM0_CURRENT_VALUE = 0x7FFF; // start value
	TIMER->TIM0_CONTROL |= TIMER_CONTROL_ENABLE; // enable timer0

	cortex_interrupt_set_priority(TIMER0_INT, 0);
	cortex_interrupt_clear(TIMER0_INT);
	cortex_interrupt_enable(TIMER0_INT);
}

int main(){
	uint8_t c;
	uint32_t delay, uptime;

	PERI_ON->CPU_PERIPHERAL_CTRL |= PERI_ON_CPU_PERIPHERAL_CTRL_SWD_PIN_EN; // re-enable SWD

	PERI_ON->PESOC_CLK_CTRL |= PERI_ON_CLK_CTRL_ACTCK_GPIO_EN | PERI_ON_CLK_CTRL_SLPCK_GPIO_EN; // enable gpio peripheral clock
	PERI_ON->SOC_PERI_FUNC1_EN |= PERI_ON_SOC_PERI_FUNC1_EN_GPIO; // enable gpio peripheral

	delay = uptime = 0;

	timer_init();

	serial_init();

	mask32_set(SYS->CLK_CTRL1, SYS_CLK_CTRL1_PESOC_OCP_CPU_CK_SEL, 1); // set system clock

	PERI_ON->GPIO_SHTDN_CTRL = 0xFF;
	PERI_ON->GPIO_DRIVING_CTRL = 0xFF;

	GPIO->SWPORTA_DDR |= GPIO_PORTA_GC4;

	interrupts_enable();

	while(1){
		if(delay++ > 1000000){
			GPIO->SWPORTA_DR ^= GPIO_PORTA_GC4;
			printf("UPTIME: %lu, TIMER: %u\n", (unsigned long int)++uptime, (unsigned int)timer);
			delay = 0;
		}
		if(serial_read(&c, 1)){
			if(c == '1'){
				printf("COMMAND 1\n");
			}else if(c == '2'){
				printf("COMMAND 2\n");
			} // ... etc
		}
	}
}

ssize_t write_stdout(const void *buf, size_t count){
	size_t i;
	for(i = 0; i < count; i++){
		if((((uint8_t *)buf)[i]) == '\n')while(!serial_write("\r", 1));
		while(!serial_write(&(((uint8_t *)buf)[i]), 1));
	}
	return(count);
}

CORTEX_ISR(TIMER0_INT){
	timer++;
	(void)TIMER->TIM0_EOI; // reading from this register clears the interrupt
	cortex_interrupt_clear(TIMER0_INT);
}

