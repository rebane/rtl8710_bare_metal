# RTL-00 bare-metal testing
for RTL8710 OpenOCD support, look: https://bitbucket.org/rebane/rtl8710_openocd  
## pins:
* UART RX: GB0  
* UART TX: GB1  
* LED:     GC4  
* SWDIO:   GE3  
* SWCLK:   GE4  
## building:
```
make
```
## testing:
```
make test
```
## flashing:
```
make flash
```

